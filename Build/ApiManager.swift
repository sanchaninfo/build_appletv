/***
 **Module Name:  ApiManager
 **File Name :  ApiManager.swift
 **Project :   Build
 **Copyright(c) : Build.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : JSON parser using Alamofire.
 */

import Foundation
import UIKit
import Alamofire

class ApiManager
{
    static let sharedManager:ApiManager = ApiManager()
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                completion(post,nil,true)
            }
            catch
            {
                completion(nil,response.result.error!,false)
            }
        }
    }
    func getDataWithJson(url:String,completion: @escaping(_ responseDict:Any?)-> Void)
    {
        Alamofire.request(url).responseJSON{
            response in
            if response.response != nil
            {
                if response.response?.statusCode == 200
                {
                    completion(response.result.value)
                }
            }
            else
            {
                completion(nil)
            }
        }
    }
}
